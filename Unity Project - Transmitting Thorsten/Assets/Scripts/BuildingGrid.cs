﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingGrid : MonoBehaviour {

    public static List<BuildingGrid> listOfBuildings;
    public static BuildingGrid furthestBuilding()
    {
        BuildingGrid choice = null;
        for (int i = 0; i < listOfBuildings.Count; i++)
        {
            if (choice == null)
            {
                choice = listOfBuildings[i];
            }
            else if (listOfBuildings[i].transform.position.x > choice.transform.position.x)
            {
                choice = listOfBuildings[i];
            }
        }

        return choice;
    }

	// Use this for initialization
	void Start () {
        if (listOfBuildings == null)
            listOfBuildings = new List<BuildingGrid>();

        listOfBuildings.Add(this);
	}

	// Update is called once per frame
	void Update () {
        var pos = transform.position;

        pos.x -= NumberSets.Reference.gridMoveSpeed;

        if (pos.x <= NumberSets.Reference.gridCapLeft)
        {
            pos.y = NumberSets.Reference.GetGridHeight();
            pos.x = NumberSets.Reference.GetGrid();
        }

        transform.position = pos;
    }

    
}
